<?
/**
 * @file
 *
 * Provide views data and handlers for taxonomy.module
 */

/**
 * Implementation of hook_views_data()
 */
function registration_views_data() {

  $data['registration_registrant']['table']['group'] = t('Registration');

  $data['registration_registrant']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  $data['registration_registrant']['uid'] = array(
    'title' => t('Registrant data'),
    'help' => t('Registrant data'),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => 'Registrant data')
    );

  // Fields that are managed as strings.
  $text_fields = array(
    'name' => array('Name', 'Name'),
    'nick' => array('Nick', 'Nickname'),
    'address_address' => array('Address', 'Address (street, road, ...)'),
    'address_zipcode' => array('Zip code', 'Zip code'),
    'address_region' => array('Region', 'Region'),
    'address_city' => array('City', 'City'),
    'phone_private' => array('Phone', 'Private phone number'),
    'phone_mobile' => array('Mobile', 'Mobile phone number'),
    'company' => array('Company', 'Company'),
    'contributes' => array('Contributes', 'FIXME'),
    'emergency' => array('Emergency', 'Contact person if an emergency arises'),
    'transport' => array('Transport', 'FIXME'),
    'remaining' => array('Remaining', 'FIXME'),
    'travel_remaining' => array('Travel remaining', 'FIXME'),
    'family' => array('Family', 'FIXME'),
    'mac_wired' => array('MAC wired', 'MAC for wired device'),
    'mac_wireless' => array('MAC wireless', 'MAC for wireless device (mobile, phone, laptop...)'),
    'dietary_requeriments' => array('Dietary requirements', 'Dietary requirements'),
    'dietary_custom_requirements' => array('Dietary custom requirements', 'Dietary custom requirements'),
  );

  // Declare the text fields defined in the $text_fields associative array.
  foreach ($text_fields as $field => $details) {
    $data['registration_registrant'][$field] = array(
      'title' => t($details[0]),
      'help' => t($details[1]),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
    );
  }

  // Fields that are managed as boolean values.
  $boolean_fields = array(
    'locked' => array('Locked', 'FIXME'),
    'needs_accommodation' => array('Accommodation', 'Travel agency must manage her accommodation'),
    'needs_car' => array('Car', 'User wants to rent a car'),
    'share_room' => array('Share room', 'User doesn\'t mind to share room'),
    'share_data' => array('Share data', 'Share registration data'),
    'ispaid' => array('Paid?', 'FIXME'),
    'announce_subscribe' => array('Announce subscribe', 'Subscribed to announce mailing list'),
    'notified_to_travel_agency' => array('Notified to travel agency', 'Users\' registration have been notified to travel agency by e-mail'),
  );

  // Declare the boolean fields defined in the $boolean_fields associative array.
  foreach ($boolean_fields as $field => $details) {
    $data['registration_registrant'][$field] = array(
      'title' => t($details[0]),
      'help' => t($details[1]),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
      ),
    );
  }

  return $data;
}
