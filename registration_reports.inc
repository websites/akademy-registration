<?php
/*
 *  Copyright 2008 Marijn Kruisselbrink <mkruisselbrink@koffice.nl>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 *   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *   IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 *   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *   NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

function registration_admin_reports() {
  $rows = array();
  $res = db_query("SELECT queryid, name FROM {registration_report_query} ORDER BY name, queryid");
  while ($row = db_fetch_object($res)) {
    $rows[] = l($row->name, 'reports/' . $row->queryid);
  }
  return theme('item_list', $rows);
}

function registration_admin_report_edit($query) {
  if ($query == 'new') {
    return drupal_get_form('registration_admin_report_form', $query);
  } else {
    return drupal_get_form('registration_admin_report_form', $query->queryid);
  }
}

function registration_admin_report_form(&$form_state, $queryid) {
  $query = db_fetch_object(db_query('SELECT * FROM {registration_report_query} WHERE queryid=%d', $queryid));

  $form = array();
  $form['name'] = array(
    '#type' => 'textfield', '#title' => 'Name', '#default_value' => $query->name
  );
  $form['sql'] = array(
    '#type' => 'textarea', '#title' => 'Query', '#default_value' => $query->sql
  );

  $form['advanced'] = array(
    '#type' => 'fieldset', '#title' => 'Advanced', '#collapsible' => true, '#collapsed' => true
  );
  $form['advanced']['rowlink'] = array(
    '#type' => 'textfield', '#title' => 'Row link', '#default_value' => $query->rowlink
  );
  $form['advanced']['linkfield'] = array(
    '#type' => 'textfield', '#title' => 'Link field', '#default_value' => $query->linkfield
  );
  $form['advanced']['paramname'] = array(
    '#type' => 'textfield', '#title' => 'Param name', '#default_value' => $query->paramname
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save')
  );
  if ($queryid != 'new') {
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#attributes' => array('class' => 'danger-button'),
    );
  }
  return $form;
}

function registration_admin_report_form_submit($form, &$form_state) {
  $queryid = arg(1);
  if ($form_state['values']['op'] == $form_state['values']['delete']) {
    registration_admin_report_delete($queryid);
    return;
  }
  if ($queryid == 'new') {
    db_query("INSERT INTO {registration_report_query} (`sql`, name, rowlink, linkfield, paramname) VALUES ('%s', '%s', '%s', '%s', '%s')", $form_state['values']['sql'], $form_state['values']['name'], $form_state['values']['rowlink'], $form_state['values']['linkfield'], $form_state['values']['paramname']);
    $queryid = db_last_insert_id('registration_report_query', 'queryid');
  } else {
    db_query("UPDATE {registration_report_query} SET `sql`='%s', name='%s', rowlink='%s', linkfield='%s', paramname='%s' WHERE queryid=%d", $form_state['values']['sql'], $form_state['values']['name'], $form_state['values']['rowlink'], $form_state['values']['linkfield'], $form_state['values']['paramname'], $queryid);
  }
  menu_rebuild();
  drupal_goto('reports/' . $queryid);
}

function registration_admin_report_delete($queryid) {
  // asking for confirmation would be nice...
  db_query("DELETE FROM {registration_report_query} WHERE queryid=%d", $queryid);
  menu_rebuild();
  drupal_goto('reports');
}

function registration_report_query_title($query) {
  return $query->name;
}

function registration_admin_report($report_query, $type = 'html') {
  $q = db_fetch_object(db_query('SELECT * FROM {registration_report_query} WHERE queryid=%d', $report_query->queryid));
  $query = $q->sql;
  $output = '<p>Running query:<br /><pre>' . $query . '</pre></p>';

  if (trim($q->paramname)) {
    $output .= '<p>' . $q->paramname . ' = ' . arg(2) . '</p>';
    $res = db_query($query, arg(2));
  } else {
    $res = db_query($query);
  }
  if (!$res) {
    $output .= '<p>The query failed to run.</p>';
    return $output;
  }
  $numrows = 0;
  $data = array();
  $indices = array();
  $maxidx = 0;
  while ($row = db_fetch_array($res)) {
    $numrows++;

    $drow = array();
    foreach ($row as $key => $val) {
      if (!isset($indices[$key])) {
        $idx = $maxidx++;
        $indices[$key] = $idx;
      } else {
        $idx = $indices[$key];
      }
      if ($type != 'csv' && $q->rowlink) {
        $val = l($val, sprintf($q->rowlink, $row[$q->linkfield]));
      }
      $drow[$idx] = $val;
    }
    $data[] = $drow;
  }

  if (!$numrows) {
    $output .= '<p>The query returned no rows.</p>';
    return $output;
  }
  $output .= '<p>The query returned ' . $numrows . ' rows.</p>';

  $header = array();
  asort($indices);
  foreach ($indices as $key => $idx) {
    $header[$idx] = $key;
  }
  if ($type == 'csv') {
    if (arg(3) == 'download' || arg(2) == 'csvdownload') {
      header('Content-Type: text/csv');
      print(theme('csvtable', $header, $data, FALSE));
      die();
    } else {
      $output .= theme('csvtable', $header, $data);
    }
  } else {
    $output .= theme('table', $header, $data);
  }

  return $output;
}

function theme_csvtable($header, $data, $html = TRUE) {
  if ($html) $res = '<pre style="overflow: auto;">';
  array_unshift($data, $header);
  foreach ($data as $row) {
    $srow = array();
    foreach ($row as $s) {
      if (strpbrk($s, ", '\"\n") !== FALSE) {
        $s = str_replace("\r", "", str_replace("\n", "\\n", $s));
        $srow[] = "'" . str_replace("'", "\\'", $s) . "'";
      } else {
        $srow[] = $s;
      }
    }
    $res .= join(",", $srow) . "\n";
  }
  if ($html) $res .= '</pre>';
  return $res;
}
