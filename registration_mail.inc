<?php
/*
 *  Copyright 2008 Marijn Kruisselbrink <mkruisselbrink@koffice.nl>
 *  Copyright 2008 Niels van Mourik <niels@shodan.nl>
 *  Copyright 2008 Bart Cerneels <bart.cerneels@kde.org>
 *  Copyright 2008 Guillermo Antonio Amaral Bastidas <gamaral@amaral.com.mx>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 *   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *   IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 *   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *   NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * This file defines some functions related with sending e-mails.
 */

/*
 * Implementation of hook_mail().
 */
function registration_mail($key, &$message, $params)
{
  $account = $params['account'];
  $message['body'] = '';

  switch ($key) {
  case 'subscription':
    $message['subject'] = 'subscribe';
    $message['body'] = '';
    break;
  case 'notification':
    _registration_notification_build($account, $message);
    $message['headers']['Cc'] = $account->mail;
    $message['headers']['Reply-To'] = $account->mail;
    $message['headers']['Bcc'] = $message['headers']['From'];
    break;
  case 'confirmation':
    _registration_confirmation_build($account, $message);
    $message['headers']['Reply-To'] = $account->mail;
    break;
  }
}

/*
 * Builds the confirmation e-mail.
 */
function _registration_confirmation_build($user, &$message)
{
  $boundary = md5(date('r'));
  $message['headers']['Content-Type'] = "multipart/mixed; boundary=$boundary";
  $registrant = $user->registrant;

  $options = _registration_load_options($user);
  $costs = registration_gather_costs($registrant);
  $accommodation = $costs['accommodation'];

  // Gather data to be sent in notification.
  $data = _registration_notification_data($user);
  $reg_id = $data['id']; // Registrant ID.

  // Build the body
  $data_for_body = array();
  foreach ($data as $key => $value) {
    $data_for_body["!$key"] = $value;
  }
  $data_for_body['!accommodation'] = _registration_accommodation_string($costs);
  $data_for_body['!options'] = _registration_options_string($options);

  $message['body'] .= "\n\n--$boundary\n";
  $message['body'] .= "Content-Type: text/plain; charset=UTF-8\n";
  $message['body'] .= "Content-Transfer-Encoding: quoted-printable\n\n";
  $message['body'] .= t(_registration_notification_message(), $data_for_body);

  $message['subject'] = 'GCDS\'09 Registration Confirmation';

  return $message;
}

/*
 * Builds the notification e-mail.
 */
function _registration_notification_build($user, &$message)
{
  $boundary = md5(date('r'));
  $message['headers']['Content-Type'] = "multipart/mixed; boundary=$boundary";
  $registrant = $user->registrant;

  $options = _registration_load_options($user);
  $costs = registration_gather_costs($registrant);
  $accommodation = $costs['accommodation'];

  // Gather data to be sent in notification.
  $data = _registration_notification_data($user);
  $reg_id = $data['id']; // Registrant ID.

  // Build the body
  $data_for_body = array();
  foreach ($data as $key => $value) {
    $data_for_body["!$key"] = $value;
  }
  $data_for_body['!accommodation'] = _registration_accommodation_string($costs);
  $data_for_body['!options'] = _registration_options_string($options);

  $message['body'] .= "\n\n--$boundary\n";
  $message['body'] .= "Content-Type: text/plain; charset=UTF-8\n";
  $message['body'] .= "Content-Transfer-Encoding: quoted-printable\n\n";
  $message['body'] .= t(_registration_notification_message(), $data_for_body);

  // Build the csv and attach it to the message.
  $csv = _registration_notification_csv($data, $accommodation, $options);
  $message['body'] .= "\n--$boundary\n";
  $message['body'] .= "Content-Type: text/csv; name=\"data-{$reg_id}.csv\"\n";
  $message['body'] .= "Content-Disposition: attachment; filename=\"data-$reg_id.csv\"\n";
  $message['body'] .= "Content-Transfer-Encoding: base64\n\n";
  $message['body'] .= chunk_split(base64_encode($csv));
  $message['body'] .= "--$boundary--\n";

  $message['subject'] = "GCDS ID: ". $reg_id; //FIXME it must be configurable.

  return $message;
}

/*
 * Builds a csv string with data to be sent attached to notifications.
 */
function _registration_notification_csv($data, $accommodation, $options = array())
{
  // FIXME: The 'fields' array must be configurable.
  $fields = array('id', 'name', 'mail', 'needs_accommodation', 'needs_trip', 'share_room', 'group_name', 'family', 'room_type', 'accommodation_type');
  $csv_data = array();
  foreach ($fields as $field) {
    $csv_data[$field] = $data[$field];
  }

  // Add one column by day.
  $days = _registration_load_days();
  foreach ($days as $day) {
    $field = date('D jS M', strtotime($day->daydate));
    $fields[] = $field;
    $csv_data[$field] = "";
  }

  // Fill the column with each day accommodation.
  foreach ($accommodation as $acc) {
    $day = $acc['day'];
    $csv_data[$day] = $acc['hostel'];
  }

  // Add options (social events and things like that).
  $fields[] = 'options';
  $csv_data['options'] = "";
  $options_names = array();
  foreach ($options as $option) {
    $options_names[] = $option->name;
  }
  $csv_data['options'] = join("+", $options_names);

  $csv_text = implode(';', $fields) . "\n";
  $csv_text .= '"' . implode('";"', $csv_data) . '"';
  return $csv_text;
}

function _registration_notification_data($user)
{
  $registrant = $user->registrant;

  // Load user related information. This could be done in hook_user :P
  if ($group = _registration_load_group($registrant->groupid)) {
    $group_name = $group->name;
  } else {
    $group_name = t('None');
  }

  if ($country = _registration_load_country($registrant->address_countryid)) {
    $country_name = $country->name;
  } else {
    $country_name = _("Error");
  }

  return array(
    'id' => $user->uid,
    'paymentid' => $user->name,
    'mail' => $user->mail,
    'name' => $registrant->name,
    'gender' => t($registrant->gender),
    'phone_private' => $registrant->phone_private,
    'phone_mobile' => $registrant->phone_mobile,
    'needs_car' => t($registrant->needs_car ? 'Yes' : 'No'),
    'needs_accommodation' => t($registrant->needs_accommodation ? 'Yes' : 'No'),
    'needs_trip' => t($registrant->needs_trip ? 'Yes' : 'No'),
    'family' => $registrant->family,
    'dietary_requirements' => $registrant->dietary_requirements . "\n" .
      $registrant->dietary_custom_requirements,
    'emergency' => $registrant->emergency,
    'group_name' => $group_name,
    'share_room' => t($registrant->share_room ? 'Yes' : 'No'),
    'address' => $registrant->address_address,
    'zip_code' => $registrant->address_zipcode,
    'city' => $registrant->address_city,
    'country' => $country_name,
    'room_type' => $registrant->room_type,
    'accommodation_type' => $registrant->accommodation_remaining,
    'attendance_remaining' => $registrant->attendance_remaining,
  );
}

/*
 * Default body for notification e-mail.
 * FIXME: It must be stored as a setting.
 */ 
function _registration_notification_message()
{
  return "ID: !id\nName: !name\nE-mail: !mail\nGender: !gender\nAddress: !address\nCity: !city\nZip code: !zip_code\nCountry: !country\nPhone: !phone_private\nNeeds car: !needs_car\nFamily: !family\nGroup: !group_name\nDietary requirements: !dietary_requirements\nEmergency: !emergency\nShare room: !share_room\nAttendance remaining: !attendance_remaining\n\n!accommodation\nRoom type: !room_type\nAccommodation type: !accommodation_remaining\n\nOptions:\n!options";
}

function _registration_accommodation_string($costs)
{
  $output .= "Reserved Accommodation:\n";
  if($costs['accommodation'])
  {
    foreach( $costs['accommodation'] as $a )
    {
      $output .= t("!hostel on !day\n", array( '!hostel' => $a['hostel'], '!day' => $a['day']));
    }
  } else {
    $output .= "You didn't book any accommodation\n";
  }
  return $output;
}

function _registration_options_string($options)
{
  $output = "";
  foreach ($options as $option) {
    $output .= "{$option->name} {$option->description}\n";
  }
  return $output;
}
