<?php

/*
 * Implementation of hook_hook_info().
 */
function registration_hook_info() {
  return array(
    'registration' => array(
      'registration' => array(
        'user_registered' => array(
          'runs when' => t('A user register for the event'),
        ),
      ),
    ),
  );
}

function registration_registration($op, $user) {
  if (!in_array($op, array('user_registered'))) {
    return;
  }

  $aids = _trigger_get_hook_aids('registration', $op);
  print_r($context);
  $context = array(
    'hook' => 'user', // needed for simplenews to work
    'account' => $user,
    'op' => $op,
  );
  actions_do(array_keys($aids), $user, $context);
}

/**
 * Implementation of hook_action_info_alter().
 */
function registration_action_info_alter(&$info) {
  foreach ($info as $type => $data) {
    if ($type == "simplenews_subscribe_user_action") {
      if (isset($info[$type]['hooks']['registration'])) {
        array_push($info[$type]['hooks']['registration'], 'user_registered');
      }
      else {
        $info[$type]['hooks']['registration'] = array('user_registered');
      }
    }
  }
}

?>
