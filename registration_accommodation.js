String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, '') }

$(function() {
    $("#edit-newgroup").parents("div:first").hide().before('<div class="form-item"><input id="edit-joingroup" class="form-submit" type="button" value="Join group" onclick="return joinGroupClicked();" /> <input id="edit-creategroup" class="form-submit" type="button" value="Create new group" onclick="return createGroupClicked();" style="display: none;" /> <input id="edit-cancelcreategroup" class="form-submit" type="button" value="Cancel" onclick="return cancelCreateGroupClicked();" style="display: none;" /><input type="hidden" name="groupswithajax" value="1" /></div><div id="groupstatus" style="display:none;" class="messages"></div>')
    $("#edit-grouppassword-wrapper").hide()
    $("#edit-grouppassword").keypress(textfieldKey)
    $("#edit-groupname").after('<p id="groupfieldlabel" style="display: none;">bla</p>').keypress(textfieldKey)
})

function textfieldKey(e) {
    if (!e) e = window.event
    if (e.keyCode == 13) {
        if ($("#edit-joingroup").is(":visible")) {
            joinGroupClicked()
        } else {
            createGroupClicked()
        }
        return false
    }
}

function joinGroupClicked() {
    $("#groupstatus").html("").removeClass("error warning").hide()
    if ($("#edit-groupname").val().trim() == "") {
        $("#groupstatus").html("Please choose a a name for the group you want to join.").addClass("error").show()
        $("#edit-groupname").addClass("error")
        $("#edit-grouppassword").removeClass("error")
        return
    }

    $.getJSON(Drupal.settings['registrationpath'] + "/ajax/joingroup", {name: $("#edit-groupname").val(), password: password = $("#edit-grouppassword").val()}, function(reply) {
        passwordfield = $("#edit-grouppassword").parents("div:first")
        if (reply['result'] == "success") {
            $("#ajax").remove()
            $("#currentgroup").html("You are currently in the group: <b><em>" + reply['groupname'] + "</em></b>.")
            $("#currentgroup").after(' <span id="ajax">\(<small><a href="javascript:leaveGroupClicked()">Leave group</a></small>\)</span>')

            $("#edit-groupname").val("")
            $("#edit-grouppassword").val("")
            $("#edit-groupname").removeClass("error")
            $("#edit-grouppassword").removeClass("error")
	    $("#edit-share-room").val("")

            if (passwordfield.is(":visible")) passwordfield.hide('fast')
            $("#groupstatus").html("You have successfully joined the group: <b><em>" + reply['groupname'] + "</em></b>.").removeClass("error").addClass("warning").show()
            document.getElementById("edit-share-room-wrapper").style.display = 'none'

        } else if (reply['result'] == "nosuchgroup") {
            $("#edit-groupname").removeClass("error").hide()
            $("#groupfieldlabel").html($("#edit-groupname").val().replace(/</g, "&lt;")).show()
            $("#edit-grouppassword").removeClass("error")
            $("#edit-joingroup").hide()
            $("#edit-creategroup").show()
            $("#edit-cancelcreategroup").show()
            passwordfield.show('fast')
            $("#groupstatus").html("No group with that name exists yet. Do you want to create a new group?").removeClass("error").addClass("warning").show()
        } else {
            if (passwordfield.is(":visible")) {
                $("#edit-groupname").removeClass("error")
                $("#edit-grouppassword").addClass("error")
                $("#groupstatus").html("The password you entered is incorrect.").removeClass("warning").addClass("error").show()
            } else {
                $("#edit-groupname").removeClass("error")
                $("#edit-grouppassword").addClass("error")
                passwordfield.show('fast')
                $("#groupstatus").html("The group you want to join requires a password.").removeClass("warning").addClass("error").show()
            }
        }
    })
}

function cancelCreateGroupClicked() {
    $("#groupstatus").html("").removeClass("error warning").hide()
    $("#edit-groupname").show()
    $("#edit-grouppassword").val("")
    $("#edit-groupname").removeClass("error")
    $("#edit-grouppassword").removeClass("error")
    if (passwordfield.is(":visible")) passwordfield.hide('fast')
    $("#edit-joingroup").show()
    $("#edit-creategroup").hide()
    $("#edit-cancelcreategroup").hide()
    $("#groupfieldlabel").hide()
}

function createGroupClicked() {
    $("#groupstatus").html("").removeClass("error warning").hide()
    if ($("#edit-groupname").val().trim() == "") {
        $("#groupstatus").html("Please choose a a name for the group you want to create.").addClass("error").show()
        $("#edit-groupname").addClass("error")
        $("#edit-grouppassword").removeClass("error")
    } else {
        $.getJSON(Drupal.settings['registrationpath'] + "/ajax/creategroup", {name: $("#edit-groupname").val(), password: password = $("#edit-grouppassword").val()}, function(reply) {
            if (reply['result'] == 'success') {
               $("#ajax").remove()
               $("#currentgroup").html("You are currently in the group: <b><em>" + reply['groupname'] + "</em></b>.")
               $("#currentgroup").after(' <span id="ajax">\(<small><a href="javascript:leaveGroupClicked()">Leave group</a></small>\)</span>')

                $("#edit-groupname").val("").show()
                $("#groupfieldlabel").hide()
                $("#edit-grouppassword").val("")
                $("#edit-groupname").removeClass("error")
                $("#edit-grouppassword").removeClass("error")
                if (passwordfield.is(":visible")) passwordfield.hide('fast')
                $("#edit-joingroup").show()
                $("#edit-creategroup").hide()
                $("#edit-cancelcreategroup").hide()
                $("#groupstatus").html("You have successfully created the group: <b><em>" + reply['groupname'] + "</em></b>.").removeClass("error").addClass("warning").show()
                document.getElementById("edit-share-room-wrapper").style.display = 'none'
            } else {
                cancelCreateGroupClicked()
                $("#groupstatus").html("A group with the name you have chosen already exists, please choose a different name or join the existing group instead.").addClass("error").show()
                $("#edit-groupname").addClass("error")
                $("#edit-grouppassword").removeClass("error")
            }
        })
    }
}

function leaveGroupClicked() {
    $("#groupstatus").html("").removeClass("error warning").hide()
    $.getJSON(Drupal.settings['registrationpath'] + "/ajax/leavegroup", {name: $("#edit-groupname").val()}, function(reply) {
	    if (reply['result'] == 'success') {
                 $("#ajax").remove()
                 $("#currentgroup").html("You are currently not in a group.")
		 $('#edit-share-room-wrapper').show()
	    }
	    else { 
	    }
        })
}

function toggleGroupings() {
  $("#groupings").toggle();
}

function reloadTab() {
   $("#spinner").show()
   $("#edit-notify-again").val('1') // Need pass this to reload
   document.getElementById("registration-accommodation-form").submit()
}

function confirmNotifyAgain() {
   $("#edit-notify-again").val('1')
   if ($("#edit-notify-again").val() == '1') {
     $("#warning-notify-again").html("Travel agency will be notified again after user data has been saved.")
   }
}
