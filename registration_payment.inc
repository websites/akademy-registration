<?php
/*
 *  Copyright 2008 Bart Cerneels <bart.cerneels@kde.org>
 *  Copyright 2008 Guillermo Antonio Amaral Bastidas <gamaral@amaral.com.mx>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 *   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *   IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 *   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *   NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

define('REGISTRATION_PAYMENT_PAYPAL', 1);
define('REGISTRATION_PAYMENT_SEPA', 2);
define('REGISTRATION_PAYMENT_SPONSORED', 3);
define('REGISTRATION_PAYMENT_POST', 4);

function registration_payment_methods() {
  return array(
    REGISTRATION_PAYMENT_PAYPAL => 'PayPal',
    REGISTRATION_PAYMENT_SEPA => 'SEPA',
    REGISTRATION_PAYMENT_SPONSORED => 'Sponsored',
    REGISTRATION_PAYMENT_POST => 'Payment on site'
  );
}

function registration_admin_payments_title($account) {
  return 'Payments for user ' . $account->registrant->name;
}

function registration_admin_payments($account) {
  return drupal_get_form('registration_admin_payments_form', $account);
}

function registration_admin_payments_form(&$form_state, $account) {
  $form = array();
  $results = db_query("SELECT method, amount FROM {registration_payments} WHERE uid=%d", arg(1));
  $received = 0;
  while ($row = db_fetch_object($results)) {
    $received += $row->amount;
    $data[] = $row;
  }

  $form['payments'] = array(
    '#type'  => 'markup',
    '#data'  => $data,
    '#theme' => 'registration_admin_payments_table',
  );

//   $form['total'] = array(
//     '#type' => 

  $form['input'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Add payment'),
    '#collapsible' => false,
    '#collapsed'   => false
  );

  $form['input']['paymentid'] = array(
    '#value' => t('<p><b>payment ID:</b> %pid</p>', array( '%pid' => $account->name)),
  );
  
  $form['input']['uid'] = array(
    '#type' => 'hidden',
    '#value' => $account->uid,
  );

  $form['input']['method'] = array(
    '#type'      => 'radios',
    '#title'     => 'payment method',
    '#default_value' => REGISTRATION_PAYMENT_SEPA,
    '#options'   => registration_payment_methods(),
  );

  $form['input']['amount'] = array(
    '#type'              => 'textfield',
    '#title'             => t('Amount'),
    '#default_value' => registration_payment_remaining($account)/100,
  );
  
  $form['input']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Add'),
  );
  
  return $form;
}

function registration_admin_payments_form_submit($form, &$form_state) {
  registration_payment_received($form_state['values']['uid'], $form_state['values']['method'], $form_state['values']['amount']*100);
}

function theme_registration_admin_payments_table($form) {
  $data = $form['#data'];
  $rows = array();
  $class = '';
  $payment_methods = registration_payment_methods();
  if( $data ) {
    foreach ($data as $row) {
      $rows[] = array('class' => $class, 'data' => array(
        sprintf( "%s %.2f", registration_variable_get( 'event_currency_sign' ),
		 $row->amount/100), $payment_methods[$row->method], ));
    }
  }

  $header = array("Amount", "Method");
  return theme('table', $header, $rows, array('style' => 'width: 100%;'));
}
