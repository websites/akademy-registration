<?php header("Content-Type: text/javascript"); ?>

function removeDay(dayid) {
    if (!confirm('Are you sure you want to remove this day "' + $('#edit-description-' + dayid).val() + '"? This action cannot be reversed.')) return
    $.getJSON(Drupal.settings['registrationpath'] + "/removeday", {'dayid': dayid}, function(reply) {
        if (reply['result'] != 'success') {
            res = confirm('Are you really sure you want to remove this day? ' + reply['error'] + '. If you continue with removing this day all registration data for this day will also be lost.')
            if (res) { // clicked Ok
                $.getJSON(Drupal.settings['registrationpath']+"/removeday", {'dayid': dayid, 'confirm': true})
                $('#dayrow' + dayid).hide()
            }
        } else {
            $('#dayrow' + dayid).hide()
        }
    })
}

function removeOption(optionid, name) {
    if (!confirm('Are you sure you want to remove this extra "' + name + '"? This action cannot be reversed.')) return
    $.getJSON( Drupal.settings['registrationpath'] + "/remove", {'optionid': optionid}, function(reply) {
        if (reply['result'] != 'success') {
            res = confirm('Are you really sure you want to remove this extra? ' + reply['error'] + '. If you continue with removing this extra all registration data for this extra will also be lost.')
            if (res) { // clicked Ok
                $.getJSON(Drupal.settings['registration.path'] + "/remove", {'optionid': optionid, 'confirm': true})
                $('#optionrow' + optionid).hide()
            }
        } else {
            $('#optionrow' + optionid).hide()
        }
    })
}

function removeAccommodation(accommodationid, name) {
    if (!confirm('Are you sure you want to remove this accommodation "' + name + '"? This action cannot be reversed.')) return
    $.getJSON(Drupal.settings['registrationpath'] + "/remove", {'accommodationid': accommodationid}, function(reply) {
        if (reply['result'] != 'success') {
            res = confirm('Are you really sure you want to remove this accommodation? ' + reply['error'] + '. If you continue with removing this accommodation all reservations for the accommodation will also be lost.')
            if (res) { // clicked Ok
                $.getJSON(Drupal.settings['registrationpath']+"/remove", {'accommodationid': accommodationid, 'confirm': true})
                $('#accommodationrow' + accommodationid).hide()
            }
        } else {
            $('#accommodationrow' + accommodationid).hide()
        }
    })
}
